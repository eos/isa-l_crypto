#!/usr/bin/env bash

EOS_CODENAME="diopside"
STCI_ROOT_PATH=/eos/project/s/storage-ci/www/eos

for BUILD_TYPE in "el-7" "al-8" "al-9" ; do
    EXPORT_DIR=${STCI_ROOT_PATH}/${EOS_CODENAME}-depend/${BUILD_TYPE}/x86_64/
    echo "Publishing for: ${BUILD_TYPE} in location: ${EXPORT_DIR}"
    mkdir -p ${EXPORT_DIR}
    cp -n ${BUILD_TYPE}_artifacts/*.rpm ${EXPORT_DIR}
    createrepo -q ${EXPORT_DIR}
done
